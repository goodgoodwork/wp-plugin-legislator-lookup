<?php
/*
Plugin Name: Legislator Lookup
Plugin URI:  https://goodgoodwork.io/plugins/legislator-lookup
Description: Adds shortcode for looking up local legislators
Version:     2017.06.06
Author:      Good Good Work
Author URI:  https:/goodgoodwork.io/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: leglu
Domain Path: /languages

Legislator Lookup is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Legislator Lookup is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Legislator Lookup. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

define( 'LEGLU_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'LEGLU_NAME', 'Legislator Lookup');
define( 'LEFLU_VERSION', '17.10.20');

// TODO register all classes here, better yet make an autoloader https://www.smashingmagazine.com/2015/05/how-to-use-autoloading-and-a-plugin-container-in-wordpress-plugins/
require_once( LEGLU_PLUGIN_PATH . 'includes/leglu-controller.php' );
require_once( LEGLU_PLUGIN_PATH . 'vendor/mustache/src/Mustache/Autoloader.php');
Mustache_Autoloader::register();
$legislatorLookup = new LegLuController;
