<?php

/**
 * Legislator Lookup controller
 *
 * @package Legislator Lookup
 * @subpackage Legislator Lookup controller
 * @since 2017.06.06
 */
class LegLuController
{
  /**
   * the google maps API key
   */
  // TODO set this via admin menu
  private $optionName = 'leglu-options';

  private $inputAddressID = 'leglu-input-address';
  private $inputSubmitID = 'leglu-input-submit';
  private $renderWrapperID = 'leglu-render-here';
  /**
   * class constructor
   */
  public function __construct()
  {
    $this->init();
  }

  /**
   * initiate object
   *
   * @return void
   */
  public function init()
  {
    // setup shortcodes and register scripts
    add_action('admin_init', array($this, 'initAdmin'));
    add_action('admin_menu', array($this, 'addOptionsPage'));
    add_action( 'wp_enqueue_scripts', array( $this, 'registerScripts' ) );
    add_action( 'wp_ajax_leglu', array( $this, 'ajaxLookup' ) );
    add_action( 'wp_ajax_nopriv_leglu', array( $this, 'ajaxLookup' ) );

    add_shortcode( 'legislator-lookup', array( $this, 'lookupShortCode' ) );
    add_shortcode( 'legislator-results', array( $this, 'reslutsShortCode' ) );
  }

  /**
   * initiate admin object
   *
   * @return void
   */
  public function initAdmin()
  {
    register_setting('leglu_options', $this->optionName, array($this, 'validateSettings'));
  }

  /**
   * add options page function
   *
   * @return void
   */
  public function addOptionsPage() {
    add_options_page( LEGLU_NAME . ' Options', LEGLU_NAME, 'manage_options', $this->optionName, array($this, 'renderOptionsPage'));
  }

  /**
   * get option from option menu or returns all opetions
   * @param  boolean $key defaults false, if set look for option in options array
   * @return returns option array or matching option $key, null if option key can't be found
   */
  public function getOption( $key = false ) {
    $result = get_option( $this->optionName );
    if ( $key ) {
      if ( isset($result[$key]) ) {
        return $result[$key];
      } else {
        return null;
      }
    }

    return $result;
  }

  /**
   * validate settings from option page
   * @param  array $input array of inputs from options page
   * @return array        array of sanatized options
   */
  public function validateSettings( $input ) {
    $valid = array(
      'leglu_maps_api_key' => sanitize_text_field($input['leglu_maps_api_key']),
      'leglu_openstates_api_key' => sanitize_text_field($input['leglu_openstates_api_key']),
      'leglu_openstates_credit' => $input['leglu_openstates_credit'],
    );

    if (strlen($valid['leglu_maps_api_key']) == 0) {
        add_settings_error(
                'leglu_maps_api_key',           // Setting title
                'leglu_texterror',              // Error ID
                __('Please enter a valid API Key','leglu'), // Error message
                'error'                         // Type of message
        );

        // Set it to blank
        $valid['leglu_maps_api_key'] = '';
    }
    if (strlen($valid['leglu_openstates_api_key']) == 0) {
        add_settings_error(
                'leglu_openstates_api_key',     // Setting title
                'leglu_texterror',              // Error ID
                __('Please enter a valid API Key','leglu'), // Error message
                'error'                         // Type of message
        );

        // Set it to blank
        $valid['leglu_openstates_api_key'] = '';
    }
    if ( isset($valid['leglu_openstates_credit']) && $valid['leglu_openstates_credit'] != 1 ) {
        add_settings_error(
                'leglu_openstates_credit',     // Setting title
                'leglu_texterror',              // Error ID
                __('Checkbox value is weird','leglu'), // Error message
                'error'                         // Type of message
        );

        // Set it to blank
        $valid['leglu_openstates_credit'] = null;
    }

    return $valid;
  }

  /**
   * registers scripts
   *
   * @return void
   */
  public function registerScripts() {
    // TODO add version number variables
    wp_register_script( 'google-places', 'https://maps.googleapis.com/maps/api/js?key=' . $this->getOption('leglu_maps_api_key') . '&libraries=places', array(), LEFLU_VERSION, true);
    wp_register_script( 'leglu-script', esc_url(plugins_url('/public/js/leglu-script.js', dirname(__FILE__) )), array('jquery','google-places'), LEFLU_VERSION, true );
    wp_register_style( 'leglu-style', esc_url(plugins_url('/public/css/leglu-style.css', dirname(__FILE__) )), array() , LEFLU_VERSION );
  }

  /**
   * enqueue scripts
   * @return void
   */
  public function enqueueScripts() {
    wp_enqueue_script('google-places');
    wp_enqueue_script('leglu-script');
    wp_enqueue_style('leglu-style');
  }

  /**
   * sends setting data to the browser as an object
   * @param array $attr array of attributes from the shortcode
   */
  public function setScriptSettings( $attr ) {
    $settings = array(
      'shortcode' => $attr,
      'ajax_url' => admin_url( 'admin-ajax.php' ),
      'nonce' => wp_create_nonce( 'leglu-nonce' ),
      'renderWrapperID' => $this->renderWrapperID,
      'inputAddressID' => $this->inputAddressID,
      'inputSubmitID' => $this->inputSubmitID
    );

    wp_localize_script( 'leglu-script', 'leglu_settings', $settings );
  }

  /**
   * initiates for lookup system when shortcode is used
   *
   * @return void
   */
  public function lookupInit()
  {
    // enque scripts
    $this->enqueueScripts();
  }

  /**
   * function that handles the ajax request for Legislator lookup
   * echo's json object which is returned to the script
   * @return void
   */
  public function ajaxLookup() {
    check_ajax_referer( 'leglu-nonce', 'nonce' );

    require_once( LEGLU_PLUGIN_PATH . 'includes/leglu-model-lookup.php' );
    $api = new LegLuModelLookup( $this->getOption('leglu_openstates_api_key') );

    $data = $_POST;

    if ( isset($data['address']) && $data['address'] != '' ) {
      $result = $api->legislatorGeoLookup( $data['lat'], $data['lng'] );
    }

    // TODO send data to LegLuModelLookup to parse data
    // TODO error handling
    if ( $result ) {
      $html = $this->renderLookupResponse( $result['body'] );
      echo json_encode( array('html' => $html ));
   } else {
     echo json_encode($data);
   }

    exit();
    // Return json data
  }

  /**
   * Construct the lookup Shortcode
   *
   * @param  array $attr attributes from shortcode
   * @return string       The HTML output for the shortcode
   */
  public function lookupShortCode( $atts ){
    require_once( LEGLU_PLUGIN_PATH . 'includes/leglu-model-input.php' );
    $model = new LegLuModelInput;

    $a = shortcode_atts( array(
      'placeholder' => $model->getAddressPlaceholder(),
      'submitValue' => $model->getInputSubmitValue(),
    ), $atts );

    $this->lookupInit();
    $this->setScriptSettings( $a );

    return $this->renderLookupInput($a);
  }

  /**
   * Construct the lookup results Shortcode
   *
   * @param  array $attr attributes from shortcode
   * @return string       The HTML output for the shortcode
   */
  public function reslutsShortCode( $atts, $content ){

    $a = shortcode_atts( array(
      'accordion' => true
    ), $atts );

    //clear empty paragraph tags
    $output = preg_replace("/^<\/p>|<p>$/", "", $content);
    $accordionClass = ($a['accordion']) ? 'accordion' : '' ;
    $credit = '';

    if ( $this->getOption('leglu_openstates_credit') != 1 ) {
      $credit = '<div class="leglu-credit">' . __('Data provided by the Open State API project','leglu') . '. ' . '<a href="https://openstates.org/funding/" target="_blank">' . __('Learn more and donate','leglu') . '</a></div>';
    }

    return '<div id="' . $this->renderWrapperID . '" style="display:none;" class="' . $accordionClass . '"><div class="leglu-content-wrapper">' . $output . '</div>' . $credit . '</div>';
  }

  /**
   * renders all content for the lookup system
   *
   * @return string returns HTML ready for display
   */
  public function renderLookupContent()
  {
    $content = array();
    $content[] = $this->renderLookupInput();
    $content[] = $this->renderLookupResult();
  }

  /**
   * render the lookup input
   *
   * @param array $data associative array of data for input view
   * @return string html content
   */
  public function renderLookupInput($data)
  {
    require_once( LEGLU_PLUGIN_PATH . 'includes/leglu-view-input.php' );
    $content = LegLuViewInput::render($data);

    // TODO: add filter for input data
    return $content;
  }

  public function renderLookupResponse( $data )
  {
    require_once( LEGLU_PLUGIN_PATH . 'includes/leglu-view-results.php' );
    $content = LegLuViewResults::render($data);

    // TODO: add filter for input data
    return $content;
  }

  /**
   * render the admin options page
   *
   * @return void simply echos the option page content
   */
  public function renderOptionsPage() {
    require_once( LEGLU_PLUGIN_PATH . 'includes/leglu-view-admin.php' );
    $content = LegLuViewInput::render($this->optionName);

    // TODO: add filter for input data
    echo $content;
  }
}
