<?php
/**
 * Legislator Lookup Admin View
 *
 * Constructs HTML for display of lookup admin options page
 * @package Legislator Lookup
 * @since 2017.06.06
 */

/**
 * class to render lookup admin
 *
 * @package Legislator Lookup
 * @since 0.1
 */
class LegLuViewInput
{
  /**
   * renders HTML
   * @param  string $optionName base name of the option
   * @return void
   */
  public static function render( $optionName ){
    $options = get_option( $optionName );
    $googleAPIUrl    = 'https://developers.google.com/maps/documentation/javascript/get-api-key';
    $openstateAPIUrl = 'https://openstates.org/api/register/';
    ob_start();
?>
<div class="wrap">
    <h2><?php echo LEGLU_NAME ?> Options</h2>
    <form method="post" action="options.php">
        <?php settings_fields('leglu_options'); ?>
        <table class="form-table">
            <tr valign="top"><th scope="row"><?php _e('Google Maps API Key','leglu') ?>:</th>
                <td>
                  <input type="text" class="large-text" name="<?php echo $optionName?>[leglu_maps_api_key]" value="<?php echo $options['leglu_maps_api_key']; ?>" />
                  <br><span class="description"><a href="<?php echo $googleAPIUrl; ?>" target="_blank"><?php _e('Get API Key here','leglu') ?></a>, <?php _e('You\'ll need a Google Maps Javasctipt API key for address lookup','leglu'); ?></span>
                </td>
            </tr>
            <tr valign="top"><th scope="row"><?php _e('Open States API Key','leglu') ?>:</th>
                <td>
                  <input type="text" class="large-text" name="<?php echo $optionName?>[leglu_openstates_api_key]" value="<?php echo $options['leglu_openstates_api_key']; ?>" />
                  <br><span class="description"><a href="<?php echo $openstateAPIUrl; ?>" target="_blank"><?php _e('Get API Key here','leglu') ?></a>, <?php _e('You\'ll need to register an Open State API Key','leglu'); ?></span>
                </td>
            </tr>
            <tr valign="top"><th scope="row"><?php _e('Hide Open State credit','leglu') ?>:</th>
                <td>
                  <input type="checkbox" name="<?php echo $optionName?>[leglu_openstates_credit]" value="1" <?php checked( isset( $options['leglu_openstates_credit'] ) ); ?> />
                  <span class="description"><?php _e('Hide the Open State API credit under search results','leglu'); ?></span>
                </td>
            </tr>
        </table>
        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
        </p>
    </form>
</div>
<?php
    return ob_get_clean();
  }

}
